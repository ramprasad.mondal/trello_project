// Common variables 
var trelloApiUrl = "https://api.trello.com/1/";
var apiKey = "your api key";
var token = "your token";

// Get boards and append it to dropdown 
var getBoards = function() {
    var url = trelloApiUrl + "members/me/boards?fields=name,url&key=" + apiKey + "&token=" + token;
    $.ajax({
        type: "GET",
        url: url,
        success: function(boards) {
            // console.log(boards)
            $.each(boards, function(index, value) {
                $('#boards')
                    .append($("<option></option>")
                        .attr("value", value.id)
                        .text(value.name));
            });
        },
        error: function(boards) {
            console.log('Boards not found');
        }
    });
}

// Get lists for a selected board
var getLists = function(boardId) {
    var url = trelloApiUrl + "boards/" + boardId + "/lists?key=" + apiKey + "&token=" + token;
    $.ajax({
        type: "GET",
        url: url,
        success: function(lists) {
            // console.log(lists)
            for (idx in lists) {
                getCards(lists[idx].id, lists[idx].name);
            }
        },
        error: function(lists) {
            console.log('Lists not found');
        }
    });
}

// Get cards for a particular list
var getCards = function(listId, listName) {
    var getCardsUrl = trelloApiUrl + "lists/" + listId + "/cards?key=" + apiKey + "&token=" + token;
    $.ajax({
        type: "GET",
        async: false,
        url: getCardsUrl,
        success: function(cards) {
            // console.log(cards)
            var $list = $('<div class="row mx-5" id="' + listName.replace(' ', '') + '"><div class="col-md-12"><h2 class="d-inline text-center">' + listName + '</h2><button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#addCardModal" data-whatever="' + listId + '">Add Card</button></div></div><hr>');
            $("#listContainer").append($list);
            drawCards(cards, listName);
        },
        error: function(cards) {
            console.log('Cards not found');
        }
    });
}

// Draw the cards
var drawCards = function(cards, listName) {
    for (var i = 0; i < cards.length; i++) {
        var $card = $(`<div class="` + cards[i].id + `"><div class="card text-white bg-secondary mb-3" style="max-width: 18rem;"><div class="card-header">` + cards[i].name + `</div><div class="card-body"><button class="btn btn-danger" onClick="deleteCardFunc('` + cards[i].id + `')">Delete</button><button type="button" class="btn btn-info" data-toggle="modal" data-target="#editCardModal" data-whatever="` + cards[i].id + `">Update Card</button></div></div></div>`);
        $("#" + listName.replace(' ', '')).append($card);
    }
}

// Add new Card
var addCardFunc = function(title, listID) {
    var getCardsUrl = trelloApiUrl + "cards?key=" + apiKey + "&token=" + token + "&name=" + title + "&idList=" + listID;
    $.ajax({
        type: "POST",
        async: false,
        url: getCardsUrl,
        success: function() {
            location.reload()
        },
        error: function() {
            console.log('Cannot add card');
        }
    });
}

// Delete Card
var deleteCardFunc = function(cardID) {
    // document.querySelector(`#${cardID}`).remove()
    var deleteCardUrl = trelloApiUrl + "cards/" + cardID + "?key=" + apiKey + "&token=" + token;
    $.ajax({
        type: "DELETE",
        async: false,
        url: deleteCardUrl,
        success: function() {
            location.reload()
        },
        error: function() {
            console.log('Cannot delete card');
        }
    });
}


// Update card name
var updateCardFunc = function(newTitle, cardID) {
    var deleteCardUrl = trelloApiUrl + "cards/" + cardID + "/name?key=" + apiKey + "&token=" + token + "&value=" + newTitle;
    $.ajax({
        type: "PUT",
        async: false,
        url: deleteCardUrl,
        success: function() {
            location.reload()
        },
        error: function() {
            console.log('Cannot delete card');
        }
    });
}

$('#boards').change(function() {
    var boardId = $("option:selected ", this).val();

    getLists(boardId);
});

$('#addCardModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var listID = button.data('whatever')
    var modal = $(this)

    document.getElementById("addCard").onclick = function() {
        var title = $('#cardTitle').val();
        if (title.trim() == '') {
            alert('Please enter card title.');
            $('#cardTitle').focus();
            return false;
        } else {
            addCardFunc(title, listID)
        }
    }
})

$('#editCardModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var cardID = button.data('whatever')
    var modal = $(this)

    document.getElementById("editCard").onclick = function() {
        var newTitle = $('#editCardTitle').val();
        if (newTitle.trim() == '') {
            alert('Please enter new card title.');
            $('#editCardTitle').focus();
            return false;
        } else {
            updateCardFunc(newTitle, cardID)
        }
    }
})